A sampling of RMs where a tool like this would have been useful:

https://en.wikipedia.org/wiki/Talk:St._Peter%27s_Square#Requested_move_1_July_2019
Saint Peter vs. St. Peter vs. St Peter

https://en.wikipedia.org/wiki/Talk:Longplay_(video_gaming)#Requested_move_14_July_2019
(video games) vs. (video gaming)

https://en.wikipedia.org/wiki/Talk:Myth_of_the_clean_Wehrmacht#Requested_move_6_June_2019
Titles with quotation marks
^Myth of

https://en.wikipedia.org/wiki/Talk:Family_tree_of_French_monarchs#Requested_move_1_July_2019

... family tree vs. Family tree of

https://en.wikipedia.org/wiki/Talk:Northumbrian_dialect#Requested_move_4_July_2019
English of...

https://en.wikipedia.org/wiki/Talk:Golden_Globe_Awards#Requested_move_10_July_2019
... Award vs. ... Awards

https://en.wikipedia.org/wiki/Talk:Meerkats_in_popular_culture#Requested_move_10_August_2019
in popular culture$ vs. Cultural depictions of


