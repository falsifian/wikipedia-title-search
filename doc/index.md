# [Wikipedia title search](..)

This tool is essentially a wrapper around Wikipedia searches using the `intitle:` keyword. For example, searching for [`raincoat`](../#raincoat) is equivalent to the search [`intitle:raincoat`](https://en.wikipedia.org/w/index.php?search=intitle%3Araincoat&title=Special%3ASearch&fulltext=1&ns0=1) on Wikipedia, with one important difference: the Wikipedia search returns 19 results, including two links to the articles "Mackintosh" and "Cagoule". You may notice these titles don't contain the text "raincoat", but they're included because there are redirects to these articles that do match the query. For example, "Macintosh raincoat" is a redirect to "Mackintosh". Our tool filters these results out, hence the text *Got 17 results. Query found 19 total hits including redirects (not shown).*.

## Query syntax

Because our tool is a wrapper around the `intitle` keyword, it supports the same syntax, as documented [here](https://www.mediawiki.org/wiki/Help:CirrusSearch#Intitle_and_incategory). Notes:

* By default, queries are subject to stemming, so a search for animals will include articles with titles containing "animal". Stop words such as "of" or "and" may be ignored.
* Multi-word phrases should be enclosed in quotation marks.
* Searches are case-insensitive, except regular expression searches.

Regular expression searches should be delimited by /.../, e.g. [/Cultural depictions of/](../#/Cultural_depictions_of/).

## Examples

* [Saint Peter vs. St. Peter vs. St Peter](../#%22saint_peter%22;/St\._Peter/;/St_Peter/)

## Known issues

* Anchors in regular expressions (`^` and `$`) don't work. E.g. [/^Cultural depictions of/](../#/^Cultural_depictions_of/) gives zero results. This seems to be a problem with the Wikipedia search API.
* Currently there's a limit of 500 search results.

## Source

Source code is at [https://gitlab.com/falsifian/wikipedia-title-search/](https://gitlab.com/falsifian/wikipedia-title-search/). By [Colin M](https://en.wikipedia.org/wiki/User:Colin_M) and [Falsifian](https://en.wikipedia.org/wiki/User:Falsifian).
