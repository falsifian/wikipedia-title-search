import React from 'react';

import * as api from './api_helpers';
import { ResultDatum } from './types';

import './styles/results.css';

const escape_page_name_for_url = (page_name : string): string => {
  // TODO: Check to see whether we need to do anything else.
  return page_name.replace(/ /g, "_");
}

// Returns the URL for the page with the given title.
// https://en.wikipedia.org/wiki/Help:URL#URLs_of_Wikipedia_pages
const url_for_title = (title: string): string => {
  return "https://en.wikipedia.org/wiki/" +
    escape_page_name_for_url(title);
}

interface TitleProps {
  title: string;
  className: string;
}

const LinkedTitle : React.FC<TitleProps> = (props: TitleProps) => {
  return (
    <a
      className={props.className}
      href={url_for_title(props.title)}
    >
      {props.title}
    </a>
  );
};

interface SearchResultProps {
  datum: ResultDatum;
}

class SearchResult extends React.Component<SearchResultProps, {}> {
  renderRedirect() {
    return (
      <li>
        <LinkedTitle
          className="redirect"
          title={this.props.datum.redirecttitle!}
        />
        <span className="rarrow">→</span>
        <LinkedTitle
          className="title"
          title={this.props.datum.title}
        />
      </li>
    );
  }

  render() {
    if (this.props.datum.redirecttitle !== undefined) {
      return this.renderRedirect();
    }
    return (
      <li>
        <LinkedTitle
          className="title"
          title={this.props.datum.title}
        />
      </li>
    );
  }
}

const NumResultsMessage : React.FC<{data: api.QueryResults}> = (props: {data: api.QueryResults}) => {
  const dat = props.data;
  let resultsTitle, maybeMore, nResultsClass = "nresults";
  if (dat.totalhits > dat.limit) {
    resultsTitle=`We fetched only the first ${dat.limit} results out of ${dat.totalhits}, due to API limits.`;
    if (props.data.isFiltered()) {
      resultsTitle += ` ${dat.limit-dat.items.length} of those were filtered out, as redirects.`;
    }
    maybeMore = (
      <span className="maybe_more"
      >
      +
      </span>
    );
    nResultsClass += " plus";
  }
  let totalHitsText;
  if (!dat.isComplete()) {
    totalHitsText = `Query found ${dat.totalhits} total hits`
    if (dat.isFiltered()) {
      totalHitsText += ' including redirects (not shown)';
    }
    totalHitsText += '.';
  }
  return (
    <p>
      Got <span
            className={nResultsClass}
            title={resultsTitle}
          >
        {dat.items.length}{maybeMore}
      </span> results. {totalHitsText}
    </p>
  );
};

interface Props {
  data: api.QueryResults;
}

export default class SearchResults extends React.Component<Props, {}> {
  render() {
    const dat = this.props.data;
    return (
    <div className="search_results">
      <NumResultsMessage data={dat}/>
      <ul>
        {dat.items.map((item : ResultDatum) => (<SearchResult key={item.title} datum={item}/>))}
      </ul>
    </div>
    );
  }
}
