import React from 'react';

import SearchPane from './SearchPane';
import { MaybeQuery } from './types';
import * as hashes from './hashes';

interface State {
  queries : Array<MaybeQuery>;
  showRedirects : boolean;
};

// A bunch of SearchPanes.
export default class Searches extends React.Component<{}, State> {
  constructor(props : {}) {
    super(props);
    const qs = hashes.decode(window.location.hash);
    this.state = {
      queries: qs,
      showRedirects: false,
    };
    this.updateStateFromHash = this.updateStateFromHash.bind(this);
  }

  componentDidMount() {
    window.addEventListener("hashchange", this.updateStateFromHash)
  }

  componentWillUnmount() {
    window.removeEventListener("hashchange", this.updateStateFromHash);
  }

  updateStateFromHash() {
    const queries = hashes.decode(window.location.hash);
    this.setState({queries});
  }

  /* Given a callback which mutates an array of queries, update the hash to reflect
     the state that results from applying that mutation to the current state.
     NB: bad things may happen if the callback removes/inserts other than at the end
     (cf. todo.txt)
  */
  updateQueries(modifyQueryList : (queries : Array<MaybeQuery>) => void) {
    const newQueries = this.state.queries.slice();
    modifyQueryList(newQueries);
    this.setQueries(newQueries);
  }
  
  setQueries(queries : Array<MaybeQuery>) {
    const hash = hashes.encode(queries);
    window.location.hash = hash;
  }

  appendQuery(newQuery : MaybeQuery) {
    this.updateQueries((qs) => qs.push(newQuery));
  }

  popQuery() {
    this.updateQueries( (qs) => {
      console.assert(qs.length > 0);
      qs.pop();
    });
  }

  setNthQuery(index:number, newQuery:MaybeQuery) {
    this.updateQueries((qs) => qs[index] = newQuery);
  }

  // TODO: would be cool if this also had the effect of focusing the search box
  reset() {
    this.setQueries([null]);
  }

  render() {
    const panes = this.state.queries.map( (q:MaybeQuery, i:number) => {
      return (
        <SearchPane
              key={i}
              query={q}
              showRedirects={this.state.showRedirects}
              onQueryTextChange={(q) => this.setNthQuery(i, q)}
              onRemove={ (i === this.state.queries.length-1) ? 
                ( () => this.popQuery() )
              : undefined
              }
         />
        );
    });
    return (
      <div className="searches">
        {panes}
        <div className="buttons">
          <BigButton
            className="newpane"
            onClick={() => this.appendQuery(null)}
            glyph="+"
            label="new pane"
          />
          <BigButton
            className="reset"
            onClick={() => this.reset()}
            glyph="x"
            label="reset"
            titleText="Clear searches and extra panes"
          />
          <div className="show_redirects">
            <label title="At most one redirect will be listed for each page">
              <input
                id="redirect_checkbox"
                type="checkbox"
                checked={this.state.showRedirects}
                onChange={(event) => this.setState({showRedirects: event.target.checked})}
              />
              Show redirects
            </label>
          </div>
        </div>
      </div>
    );
  }
}

interface BBProps {
  glyph: string;
  label: string;
  onClick: (evt: any) => void;
  className: string;
  titleText?: string;
}

const BigButton: React.FC<BBProps> = (props: BBProps) => {
  return (
  <button 
    className={"big_button " + props.className}
    onClick={props.onClick}
    title={props.titleText}
  >
    <div className="glyph">{props.glyph}</div>
    <label>{props.label}</label>
  </button>
  );
}
