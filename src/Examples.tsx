import React from 'react';

import './styles/examples.css';

interface ExampleData {
  query: string;
  comment: string;
}

const EXAMPLES: Array<ExampleData> = [
  {
    query: 'platypus',
    comment: 'Titles containing "platypus" (case-insensitive).',
  },
  // TODO: Multi-word example(s) with and without quotes.
  {
    query: '/List of .* hurricanes/',
    comment: 'Regular expression search. Matches "List of Florida hurricanes", "List of New England hurricanes", etc.',
  }
];

const Example: React.FC<{example: ExampleData}> = (props: {example: ExampleData}) => (
  <li className="example">
    <code className="query">{props.example.query}</code>
    <span className="comment">{props.example.comment}</span>
  </li>
);

interface State {
  expanded: boolean;
}

// A component that shows some example queries.
export default class Examples extends React.Component<{}, State> {
  constructor(props: {}) {
    super(props);
    this.state = {expanded: false};
  }

  toggleExpanded() {
    this.setState((state) => ({expanded: !state.expanded}));
  }

  render() {
    const example_elements = EXAMPLES.map((example) => (
      <Example
        key={example.query}
        example={example}
      />));
    return (
      <div className="examples">
        <span className="examples_button"
          onClick={() => this.toggleExpanded()}
        >
          Example queries [{this.state.expanded ? '-' : '+'}]
        </span>
        <ul className={"example_list" + (this.state.expanded ? "" : " hidden")}>
          {example_elements}
        </ul>
      </div>
    );
  }
}
