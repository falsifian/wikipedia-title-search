import React from 'react';

import { MaybeQuery } from './types';

interface Props {
  initialQuery: MaybeQuery;
  onSearch : (query : MaybeQuery) => void;
}

interface State {
  editedQueryText: string;
}

export default class SearchControls extends React.Component<Props, State> {
  constructor(props : Props) {
    super(props);
    this.state = {
      editedQueryText: props.initialQuery || '',
    };
  }

  componentDidUpdate(oldprops: Props, oldstate: State) {
    // If this happens, we've received a change to the corresponding SearchPane's
    // query originating from a change in the URL. For example, on back/forward.
    if (oldprops.initialQuery !== this.props.initialQuery) {
      this.setState({editedQueryText: this.props.initialQuery || ''});
    }
  }

  render() {
    return (
      <form>
        <input
           type="search"
           value={this.state.editedQueryText}
           onChange={(event) => this.setState({editedQueryText: event.target.value})}
         />
        <button onClick={(event : any) => {
          event.preventDefault();
          this.props.onSearch(this.state.editedQueryText || null);
        }}>Search</button>
      </form>
    );
  }
}
