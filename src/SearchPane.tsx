import React from 'react';

import SearchControls from './SearchControls';
import SearchResults from './SearchResults';
import * as api from './api_helpers';
import { MaybeQuery } from './types';

interface Props {
  query : MaybeQuery;
  showRedirects : boolean;
  onQueryTextChange : (newQuery : MaybeQuery) => void;
  onRemove?: () => void;
}

interface State {
  // Undefined when we haven't received a response from the API (including when props.query === null).
  resultData? : api.ApiResult;
}

export default class SearchPane extends React.Component<Props, State> {
  constructor(props : Props) {
    super(props);
    this.state = {
      resultData: undefined,
    };
  }

  componentDidMount() {
    if (this.props.query !== null) {
      this.doSearch();
    }
  }

  componentDidUpdate(oldprops:Props, oldstate:State) {
    if (oldprops.query !== this.props.query) {
      // clear any stale results from previous query
      this.setState({resultData: undefined});
      if (this.props.query !== null) {
        this.doSearch();
      }
    }
  }

  doSearch() {
    console.assert(this.props.query !== null);
    const srprop = ["size", "wordcount", "timestamp", "redirecttitle"];
    const searchParams = {
      action: "query",
      list: "search",
      srsearch: 'intitle:' + this.props.query + '',
      srlimit: 500,
      srnamespace: 0,
      srprop: srprop.join("|"),
      format: "json",
      origin: "*",
      // TODO: configurable sort keys would go here, e.g.:
      //srsort: "create_timestamp_asc",
    };
    const promise = api.search(searchParams);
    const query = this.props.query;
    promise.then(api_result => this.setState((state : State, props : Props) => {
        if (props.query === query) {
          return {resultData: api_result};
        } else {
          return null;
        }
    }));
  }

  render() {
    let results;
    if (this.state.resultData) {
      if (api.isError(this.state.resultData)) {
        results = <span className="api_error">{this.state.resultData.message}</span>;
      } else {
        results = (
          <SearchResults 
            data={this.state.resultData.filter(this.props.showRedirects)}
          />);
      }
    } else if (this.props.query !== null) {
      results = "Loading...";
    } else {
      results = null;
    }
    return (
      <div className="search_pane">
        {this.props.onRemove !== undefined &&
          <button className="remove"
            title="Remove pane"
            onClick={(evt) => this.props.onRemove!()}
          >
            X
          </button>
        }
        <SearchControls
          initialQuery={this.props.query}
          onSearch={this.props.onQueryTextChange}
        />
        {results}
      </div>
    );
  }
}
