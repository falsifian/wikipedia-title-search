import { ResultDatum } from './types';

export class QueryResults {
  constructor(
    readonly limit:number,
    // Will include redirects even if excluded from items.
    readonly totalhits: number,
    // May exclude redirects.
    readonly items: Array<ResultDatum>,
  ) {}
  static fromJson(json: any, searchParams:any): QueryResults {
    const query = json.query;
    return new QueryResults(
      searchParams.srlimit,
      query.searchinfo.totalhits,
      query.search,
    );
  }

  filter(showRedirects: boolean): QueryResults {
    if (showRedirects) {
      return this;
    } else {
      const newItems = this.items.filter((item: any) => !item.redirecttitle);
      return new QueryResults(this.limit, this.totalhits, newItems);
    }
  }

  isFiltered(): boolean {
    return Math.min(this.totalhits, this.limit) > this.items.length;
  }

  isComplete(): boolean {
    return this.items.length === this.totalhits;
  }

  // TODO: addResults(json:any, searchParams:any)
}

export interface Error {
  // Message to show to the user.
  message: string;
}

export type ApiResult = QueryResults | Error;

export const isError = (result: ApiResult) : result is Error => {
  return "message" in result;
}

const resultFromJson = (json: object, params: any) : ApiResult => {
  if ('error' in json) {
    return {message: JSON.stringify(json)};
  } else {
    return QueryResults.fromJson(json, params);
  }
}

// Excludes redirects.
export const search = (params : any) : Promise<ApiResult> => {
  const paramsObject = new URLSearchParams(params);
  const url = "https://en.wikipedia.org/w/api.php?" + paramsObject.toString();
  const promise = window
     .fetch(url)
     .then(response => response.json())
     .then(json => resultFromJson(json, params), 
           reason => ({message: "Error parsing response json: " + reason})
          );
  return promise;
};
