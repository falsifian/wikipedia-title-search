import React from 'react';
import './App.css';

import Examples from './Examples';
import Searches from './Searches';

const App: React.FC = () => {
  return (
    <div className="App">
      <Searches/>
      <a href="doc">Help</a>
      <Examples/>
    </div>
  );
}

export default App;
