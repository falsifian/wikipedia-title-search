/* Encoding application state into the hash part of URL (and decoding)
*/
import { MaybeQuery } from './types';

const SEP = ';';
const PERCENT_ENCODED_CHARACTERS = new Set([
  SEP, 
  // we use this to encode spaces (for aesthetic reasons)
  '_',
  // so we don't get confused by % encoding
  '%', 
  // reserved for future use
  '~', 
  // Because square brackets are used for wikitext external links
  ']', '[',
]);

function encodeQuery(q: MaybeQuery) : string {
  console.assert(q !== '');
  if (q === null) {
    return '';
  }
  let encoded = [];
  for (let c of q) {
    if (c === ' ') {
      // This could cause some confusion when encoding _, but _ is not usually
      // used in page titles so it shouldn't be an issue.
      encoded.push('_');
    } else if (PERCENT_ENCODED_CHARACTERS.has(c)) {
      const code_point = c.codePointAt(0);
      // Assumes c will be >15 and <256, so that the encoding is two digits.
      encoded.push(`%${code_point!.toString(16)}`);
    } else {
      // The browser may %-encode some other characters.
      encoded.push(c);
    }
  }
  return encoded.join('');
}

function decodeQuery(s: string) : MaybeQuery {
  if (s === '') {
    return null;
  }
  return decodeURIComponent(s.replace(/_/g, ' '));
}

export function encode(queries : Array<MaybeQuery>) : string {
  if (queries.length === 1 && queries[0] === null) {
    return '';
  }
  return '#' + queries.map(encodeQuery).join(SEP);
}

export function decode(hash: string) : Array<MaybeQuery> {
  if (hash === '') {
    return [null];
  }
  console.assert(hash && hash[0] === '#');
  const parts = hash.substr(1).split(SEP);
  return parts.map(decodeQuery);
}
