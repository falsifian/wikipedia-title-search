// A single article result from the search API.
export interface ResultDatum {
  title: string;
  redirecttitle?: string;
}

export type Query = string; 

export type MaybeQuery = Query | null;
