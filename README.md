Tool for searching Wikipedia page titles.

To try it out as you make changes: run `npm start`. You will need to run `npm install` from this directory the first time.

# Deploying

To deploy to https://tools.wmflabs.org/title-search/v0 :

You'll need to run the first few steps on a computer with a new enough version of npm, i.e. probably not toolforge:

1. If you've updated documentation, follow the instructions in `doc/README` first.
1. Check out the `site` branch and `git merge master`.
1. `npm run build`, then git add and commit the changes. (The `site` branch contains a change to `.gitignore` that causes changes to the `build` directory to be included.)
1. Push your changes.

Now, on a toolforge ssh session:

1. `become title-search`
1. `cd wikipedia-title-search`
1. Make sure you're on the `site` branch, and pull your changes.
1. `rsync -r --delete build/ ~/public_html/(version)` where `(version)` is whatever the current version identifier is, e.g. `v0`, `v1`. (The version identifier is intended to be updated if we make backward-incompatible changes in the URL scheme.)
1. You may need to run `webservice start` if the web server stopped for some reason. Other useful commands: `webservice restart` and `webservice stop`.
